﻿using System;
using System.Collections.Generic;

namespace ConsoleGameEPAM
{
    class Program
    {
        public static List<string[]> frame = new List<string[]>();
        public static bool gameStatus = true;
        public static string hero = "X";
        public static string wall = "#";
        public static string target = "%";
        public static string emptyCell = " ";

        private static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;

            Program.frame.Add(new string[12] { "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#" });
            Program.frame.Add(new string[12] { "#", "X", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#" });
            Program.frame.Add(new string[12] { "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", "%", "#" });
            Program.frame.Add(new string[12] { "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#" });

            Program.render();
            while (Program.gameStatus)
            {
                Program.moveHero(Console.ReadKey());
                Program.render();
            }
        }

        private static void youWin()
        {
            Console.Clear();
            Console.WriteLine("YOU WIN!!!");
            Console.Beep();
            Program.gameStatus = false;
            Console.ReadKey();
        }

        private static void render()
        {
            Console.Clear();
            for (int index = 0; index < Program.frame.Count; ++index)
                Console.WriteLine(string.Join("", Program.frame[index]));
        }

        private static void moveHero(ConsoleKeyInfo keyInfo)
        {
            for (int index1 = Program.frame.Count - 1; index1 >= 0; --index1)
            {
                for (int index2 = 0; index2 < Program.frame[index1].Length; ++index2)
                {
                    if (Program.frame[index1][index2] == Program.hero)
                    {
                        if (keyInfo.Key == ConsoleKey.UpArrow)
                        {
                            if (index1 - 1 >= 0 && Program.frame[index1 - 1][index2] == Program.emptyCell)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1 - 1][index2] = Program.hero;
                                return;
                            }
                            if (index1 - 1 >= 0 && Program.frame[index1 - 1][index2] == Program.target)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1 - 1][index2] = Program.hero;
                                Program.youWin();
                                return;
                            }
                        }
                        else if (keyInfo.Key == ConsoleKey.DownArrow)
                        {
                            if (index1 + 1 <= Program.frame.Count - 1 && Program.frame[index1 + 1][index2] == Program.emptyCell)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1 + 1][index2] = Program.hero;
                                return;
                            }
                            if (index1 + 1 <= Program.frame.Count - 1 && Program.frame[index1 + 1][index2] == Program.target)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1 + 1][index2] = Program.hero;
                                Program.youWin();
                                return;
                            }
                        }
                        else if (keyInfo.Key == ConsoleKey.LeftArrow)
                        {
                            if (index2 - 1 >= 0 && Program.frame[index1][index2 - 1] == Program.emptyCell)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1][index2 - 1] = Program.hero;
                                return;
                            }
                            if (index2 - 1 >= 0 && Program.frame[index1][index2 - 1] == Program.target)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1][index2 - 1] = Program.hero;
                                Program.youWin();
                                return;
                            }
                        }
                        else if (keyInfo.Key == ConsoleKey.RightArrow)
                        {
                            if (index2 + 1 <= Program.frame[index1].Length - 1 && Program.frame[index1][index2 + 1] == Program.emptyCell)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1][index2 + 1] = Program.hero;
                                return;
                            }
                            if (index2 + 1 <= Program.frame[index1].Length - 1 && Program.frame[index1][index2 + 1] == Program.target)
                            {
                                Program.frame[index1][index2] = Program.emptyCell;
                                Program.frame[index1][index2 + 1] = Program.hero;
                                Program.youWin();
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
